from django import forms
from main.models import *


class ArticleForm(forms.ModelForm):
	title = title = models.CharField(max_length=Article.TITLE_MAX_LENGTH, blank=False, unique=True)
	image = models.ImageField(upload_to='article_images', blank=True)
	text = models.TextField(blank=False)
	slug = forms.CharField(widget=forms.HiddenInput(), required=False)

	class Meta:
		model = Article
		fields = ('title', 'image', 'text', )


class CommentForm(forms.ModelForm):
	text = models.TextField(blank=False)

	class Meta:
		model = Comment
		fields = ('text', )