from django.contrib import admin
from main.models import *
# Register your models here.

class ArticleAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title', )}


admin.site.register(Article, ArticleAdmin)
