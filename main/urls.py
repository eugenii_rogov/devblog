from django.urls import path
from main import views

app_name = 'blog'

urlpatterns = [
	path('', views.index, name='index'),
	path('add_article/', views.add_article, name='add_article'),
	path('article/<slug:article_title_slug>/', views.show_article, name='show_article'),
	path('article/<slug:article_title_slug>/delete_article/', views.delete_article, name='delete_article'),
	path('article/<slug:article_title_slug>/edit_article/', views.edit_article, name='edit_article'),
	path('article/<slug:article_title_slug>/add_like/', views.add_like, name='add_like'),
	path('article/<slug:article_title_slug>/add_lislike/', views.add_dislike, name='add_dislike'),
	path('article/<slug:article_title_slug>/add_comment/', views.add_comment, name='add_comment'),
	path('article/<slug:article_title_slug>/delete_comment/<slug:comment_id>', views.delete_comment, name='delete_comment'),
	path('article/<slug:article_title_slug>/<slug:comment_id>/add_like', views.add_comment_like, name='add_comment_like'),
	path('article/<slug:article_title_slug>/<slug:comment_id>/add_comment_dislike', views.add_comment_dislike, name='add_comment_dislike'),

]