from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.urls import reverse
from main.models import Article, Like, Comment, CommentLike, CommentDislike
from main.forms import *
# Create your views here.


# Render main page with articles
def index(request):
	context_dict = {}
	try:
		context_dict['articles'] = Article.objects.all()
	except:
		context_dict['articles'] = None
	return render(request, 'blog/index.html', context=context_dict)


# Func for render form, after POST request, if form is valid, create new article
def add_article(request):
	form = ArticleForm()

	if request.method == 'POST':
		form = ArticleForm(request.POST)
		if form.is_valid():
			article = form.save(commit=False)
			article.user = request.user
			if 'image' in request.FILES:
				article.image = request.FILES['image']

			article.save()
			return redirect(reverse('blog:index'))
		else:
			print(form.errors)
	return render(request, 'blog/add_article.html', {'form': form})


# Func for render full info about article
def show_article(request, article_title_slug):
	context_dict = {}
	try:
		article = Article.objects.get(slug=article_title_slug)
		context_dict['article'] = article
		context_dict['comments'] = Comment.objects.filter(article=article)
		context_dict['likes'] = Like.objects.filter(article=article).count()
		context_dict['dislike'] = Dislike.objects.filter(article=article).count()
	except Article.DoesNotExist:
		context_dict['article'] = None
	return render(request, 'blog/show_article.html', context=context_dict)


# Func for article delete
def delete_article(request, article_title_slug):
	try:
		article = Article.objects.get(slug=article_title_slug)
		article.delete()
	except Article.DoesNotExist:
		article = None
	return redirect(reverse('blog:index'))


# Func for article edit
def edit_article(request, article_title_slug):
	try:
		user = request.user
		article = Article.objects.get(slug=article_title_slug)
	except Article.DoesNotExist:
		article = None

	if request.method == 'POST':
		form = ArticleForm(request.POST, instance=article)
		if form.is_valid():
			article = form.save(commit=False)

			if 'image' in request.FILES:
				article.image = request.FILES['image']

			article.save()
			return redirect(reverse('blog:show_article', kwargs={'article_title_slug': article_title_slug}))
		else:
			print(form.errors)
	else:
		form = ArticleForm(instance=article)
	return render(request, 'blog/edit_article.html', {'article': article,'form': form})


# Func for like adding
def add_like(request, article_title_slug):
	try:
		article = Article.objects.get(slug=article_title_slug)
		user = request.user
	except:
		article = None
		user = None

	try:
		like = Like.objects.get(user=user, article=article)
	except Like.DoesNotExist:
		like = None

	try:
		dislike = Dislike.objects.get(user=user, article=article)
	except Dislike.DoesNotExist:
		dislike = None

	if like:
		pass
	else:
		if article and user:
			if dislike:
				dislike.delete()
			like = Like(user=user, article=article).save()

	return redirect(reverse('blog:show_article', kwargs={'article_title_slug': article_title_slug}))


# Func for dislike adding
def add_dislike(request, article_title_slug):
	try:
		article = Article.objects.get(slug=article_title_slug)
		user = request.user
	except:
		article = None
		user = None

	try:
		like = Like.objects.get(user=user, article=article)
	except Like.DoesNotExist:
		like = None

	try:
		dislike = Dislike.objects.get(user=user, article=article)
	except Dislike.DoesNotExist:
		dislike = None

	if dislike:
		pass
	else:
		if article and user:
			if like:
				like.delete()
			dislike = Dislike(user=user, article=article).save()

	return redirect(reverse('blog:show_article', kwargs={'article_title_slug': article_title_slug}))


# Func for comment adding
def add_comment(request, article_title_slug):
	try:
		article = Article.objects.get(slug=article_title_slug)
		user = request.user
	except:
		article = None
		user = None


	if request.method == 'POST':
		if article and user:
			form = CommentForm(request.POST)
			if form.is_valid():
				comment = form.save(commit=False)
				comment.user = user
				comment.article = article
				comment.save()
				return redirect(reverse('blog:show_article', kwargs={'article_title_slug': article_title_slug}))
			else:
				print(form.errors)
	else:
		form = CommentForm()
	return render(request, 'blog/add_comment.html', {'article': article, 'form': form})


# Func for comment deleting
def delete_comment(request, article_title_slug, comment_id):
	try:
		article = Article.objects.get(slug=article_title_slug)
		user = request.user
	except:
		article = None
		user = None

	try:
		comment = Comment.objects.get(id=comment_id, user=user)
	except:
		comment = None

	if user and article and comment:
		comment.delete()

	return redirect(reverse('blog:show_article', kwargs={'article_title_slug': article_title_slug}))


# Func for comment like adding
def add_comment_like(request, article_title_slug, comment_id):
	try:
		article = Article.objects.get(slug=article_title_slug)
		user = request.user
	except:
		article = None
		user = None

	try:
		comment = Comment.objects.get(id=comment_id)
	except:
		comment = None

	try:
		comment_like = CommentLike.objects.get(comment=comment, user=user)
	except:
		comment_like = None

	try:
		comment_dislike = CommentDislike.objects.get(comment=comment, user=user)
	except:
		comment_dislike = None

	if user and article:
		if comment_dislike:
			comment_dislike.delete()
		if not comment_like:
			comment_like = CommentLike(comment=comment, user=user).save()

	return redirect(reverse('blog:show_article', kwargs={'article_title_slug': article_title_slug}))


# Func for comment like adding
def add_comment_dislike(request, article_title_slug, comment_id):
	try:
		article = Article.objects.get(slug=article_title_slug)
		user = request.user
	except:
		article = None
		user = None

	try:
		comment = Comment.objects.get(id=comment_id)
	except:
		comment = None

	try:
		comment_like = CommentLike.objects.get(comment=comment, user=user)
	except:
		comment_like = None

	try:
		comment_dislike = CommentDislike.objects.get(comment=comment, user=user)
	except:
		comment_dislike = None

	print('deb')
	if user and article:
		if comment_like:
			print('c_l delted')
			comment_like.delete()
		if not comment_dislike:
			comment_dislike = CommentDislike(comment=comment, user=user).save()

	return redirect(reverse('blog:show_article', kwargs={'article_title_slug': article_title_slug}))




















