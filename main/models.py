from django.db import models
from django.template.defaultfilters import slugify
from PIL import Image
from django.contrib.auth.models import User
# Create your models here.

class Article(models.Model):
	TITLE_MAX_LENGTH = 128

	title = models.CharField(max_length=TITLE_MAX_LENGTH, blank=False, unique=True)
	image = models.ImageField(upload_to='article_images', blank=True)
	text = models.TextField(blank=False)
	slug = models.SlugField(unique=True)
	user = models.ForeignKey(User, on_delete=models.CASCADE)

	def save(self, *args, **kwargs):
		self.slug = slugify(self.title)
		super(Article, self).save(*args, **kwargs)

		try:
			img = Image.open(self.image.path)
		except:
			img = None

		if img:
			if img.width > 780 or img.height > 600:
				output_size = (700, 600)
				img.thumbnail(output_size, Image.ANTIALIAS)
				print(self.image.path)
				img.save(self.image.path, quality=60)
				super(Article, self).save(*args, **kwargs)

	def __str__(self):
		return self.title


class Like(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	article = models.ForeignKey(Article, on_delete=models.CASCADE)


class Dislike(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	article = models.ForeignKey(Article, on_delete=models.CASCADE)


class Comment(models.Model):
	text = models.TextField(blank=False)
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	article = models.ForeignKey(Article, on_delete=models.CASCADE)


class CommentLike(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	comment = models.ForeignKey(Comment, related_name='comment_likes', on_delete=models.CASCADE)


class CommentDislike(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	comment = models.ForeignKey(Comment, related_name='comment_dislikes', on_delete=models.CASCADE)
