import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dev_blog.settings')
import django
django.setup()
from main.models import Article, User

def populate():
	articles = [
		{
			'title': 'Что помешало экипажу Crew Dragon выйти из корабля?',
			'text': 'Вчера, во время трансляции посадки американского космического корабля Crew Dragon, многие обратили внимание на заминку, которая возникла перед открытием бокового люка. Газоанализатор показал превышение концентрации тетраоксида азота — токсичного топливного компонента двигателей корабля. Астронавтам пришлось полчаса ждать чтобы химия выветрилась, а команда спасателей это время провела в противогазах.',
			'image': 'dragon_crew_old.jpeg'
		},
		{
			'title': 'Время первых',
			'text': '6 августа 1991 года можно считать днём рождения сети Интернет. В этот день Тим Бернерс-Ли запустил первый в мире веб-сайт на первом в мире веб-сервере, доступном по адресу info.cern.ch. Ресурс определял понятие «Всемирной паутины», содержал инструкции по установке веб-сервера, использования браузера и т.п. Этот сайт также являлся первым в мире интернет-каталогом, потому что позже Тим Бернерс-Ли разместил и поддерживал там список ссылок на другие сайты. Это было знаковое начало, которое сделало интернет таким, каким мы его знаем сейчас.Мы не видим повода, чтобы не выпить не вспомнить другие первые события в мире Интернета. Правда, статья писалась и вычитывалась с холодком: страшно осознавать, что некоторые коллеги младше, чем первый сайт и даже первый мессенджер, а ты сам добрую половину этого помнишь как часть своей биографии. Эй, время, когда мы успели повзрослеть?',
			'image': 'time_for_first.png'
		}
	]

	users = [
		{
			'username': 'Alina',
			'email': 'Alina@mail.ru',
			'password': '12345678Qwe',
		},
		{
			'username': 'Han Solo',
			'email': 'Solo@forever.com',
			'password': 'sololololo!2'
		},
		{
			'username': 'Naruto',
			'email': 'Naruto@Hokage.mail.jp',
			'password': 'naruuuutooo!3'
		}
	]

	# def add_article(title, text, image):
	# 	art, is_created = Article.objects.get_or_create(title=title, text=text, image=image)
	# 	if not is_created:
	# 		art.save()
	# 	return art

	# for art in articles:
	# 	add_article(art['title'], art['text'], art['image'])


	def add_user(username, email, password):
		user, is_created = User.objects.get_or_create(username=username, email=email)
		if not is_created:
			user.set_password(password)
			user.save()
		return user

	for user in users:
		add_user(user['username'], user['email'], user['password'])


if __name__ == '__main__':
    print('Starting DevBlog population script')
    populate()
